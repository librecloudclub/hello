# Welcome to Libre Cloud Club [WIP , Please contribute]

This page, contains all the details about this collective. 


Libre Cloud Club is a collective that aims to run Libre Software Services as a Software Substitute(SaaSS). We primarily are people in and around southern India. 

The [mailing list](https://www.freelists.org/list/librecloudclub) is a public list, where all discussions around the things we do would take place. 

All decisions for this list will take place in a democratic decision making FOSS platform called Loomio hosted [here](https://codema.in/join/group/8V52o8YJrFFCakRpMg9pYKZ5/). If you would to participate in decision making, please do subscribe to it. 

All documentation can be found in this git group.


Domain Name : https://librecloud.club


Mailing List : https://www.freelists.org/list/librecloudclub


Decision Making Platform : https://codema.in/join/group/8V52o8YJrFFCakRpMg9pYKZ5/ (Approval process. All members can approve)



Some etiquette's of this collective. 

1. All are equal. 
2. Everyone is free to participate in discussions and decision making.
3. Democratic functioning. 4. Libre only. 
